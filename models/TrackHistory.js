const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TrackHistorySchema = new Schema({
    userID: {
        type: String,
        required: true
    },
    trackID: {
        type: String,
        required: true
    },
    dateTime: {
        type: String,
        required: true
    }
});

const TrackHistory = mongoose.model('TrackHistory', TrackHistorySchema);

module.exports = TrackHistory;