const express = require('express');
const User = require('../models/User');
const TrackHistory = require('../models/TrackHistory');

const createRouter = () => {
    const router = express.Router();

    router.post('/', async (req, res) => {
        const token = req.get('Token');
        const historyData = req.body;

        if (!token) res.status(401).send({error: 'Token is not present!'});

        const user = await User.findOne({token: token});

        if (!user) res.status(401).send({error: 'User not found!'});

        historyData.userID = user._id;
        historyData.dateTime = new Date().toISOString();
        const history = new TrackHistory(historyData);

        await history.save();

        return res.send(history);
    });

    return router;
};

module.exports = createRouter;

